use std::collections::BTreeMap;

use egui::{Color32, Label, RichText, TextEdit};
use egui_extras::{Column, TableBuilder};
use ehttp::Request;
use serde::Deserialize;
use serde_json::Value;

// TODO: Find a way to use `ok` as tag
#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum ApiResponse {
    Success { prices: BTreeMap<String, Station> },
    Failure { message: String },
}

#[derive(Debug, Deserialize)]
#[serde(tag = "status", rename_all = "lowercase")]
enum Station {
    Open {
        #[serde(deserialize_with = "deserialize_price")]
        e5: Option<f64>,
        #[serde(deserialize_with = "deserialize_price")]
        e10: Option<f64>,
        #[serde(deserialize_with = "deserialize_price")]
        diesel: Option<f64>,
    },
    Closed,
    #[serde(rename = "no prices")]
    NoPrices,
}

fn deserialize_price<'de, D>(deserializer: D) -> Result<Option<f64>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let value: Value = Deserialize::deserialize(deserializer)?;
    match value.as_f64() {
        Some(n) => Ok(Some(n)),
        None => match value {
            Value::Bool(false) => Ok(None),
            _ => Err(serde::de::Error::custom("expected false or f64")),
        },
    }
}

#[derive(Debug, thiserror::Error)]
enum ApiError {
    #[error("received garbled response from server: {0}")]
    Serde(#[from] serde_json::Error),

    #[error("ehttp: {0}")]
    EHttp(String),
}

#[derive(Debug, Default)]
pub struct App {
    api_key: String,
    data: Option<Result<ApiResponse, ApiError>>,
}

impl App {
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        Default::default()
    }

    fn fetch_data(&mut self) {
        let response = ehttp::fetch_blocking(&Request::get(format!("https://creativecommons.tankerkoenig.de/json/prices.php?ids=278130b1-e062-4a0f-80cc-19e486b4c024,474e5046-deaf-4f9b-9a32-9797b778f047&apikey={}", self.api_key)));
        self.data = Some(
            response
                .map_err(ApiError::EHttp)
                .and_then(|data| data.json::<ApiResponse>().map_err(Into::into)),
        );
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Tanker Koenig");

            ui.add_space(8.0);

            ui.horizontal(|ui| {
                ui.label("API key: ");
                ui.add(TextEdit::singleline(&mut self.api_key).password(true));
            });

            ui.add_space(16.0);

            if ui.button("Fetch data").clicked() {
                self.fetch_data()
            }

            ui.add_space(32.0);

            match &self.data {
                None => {
                    stations_tabel(ui, 0, std::iter::empty());
                }
                Some(Err(e)) => {
                    ui.colored_label(
                        Color32::RED,
                        format!("There was an error fetching data from the API: {}", e),
                    );
                }
                Some(Ok(ApiResponse::Failure { message })) => {
                    ui.colored_label(
                        Color32::RED,
                        format!("There was an error fetching data from the API: {}", message),
                    );
                }
                Some(Ok(ApiResponse::Success { prices })) => {
                    stations_tabel(ui, prices.len(), prices.iter())
                }
            }

            ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui| {
                powered_by_egui_and_eframe(ui);
                egui::warn_if_debug_build(ui);
            });
        });
    }
}

fn stations_tabel<'a, I>(ui: &mut egui::Ui, stations_len: usize, mut stations: I)
where
    I: Iterator<Item = (&'a String, &'a Station)>,
{
    TableBuilder::new(ui)
        .striped(true)
        .columns(Column::auto(), 5)
        .header(20.0, |mut header| {
            for title in ["ID", "Status", "E5", "E10", "Diesel"] {
                header.col(|ui| {
                    ui.add(Label::new(RichText::new(title).strong()).wrap(false));
                });
            }
        })
        .body(|body| {
            body.rows(18.0, stations_len, |mut row| {
                // Unwrap is safe here because we checked using len() before
                let (id, station) = stations.next().unwrap();
                row.col(|ui| {
                    ui.add(Label::new(id).wrap(false));
                });

                let (status, e5, e10, diesel) = match station {
                    Station::Open { e5, e10, diesel } => ("Open", *e5, *e10, *diesel),
                    Station::Closed => ("Closed", None, None, None),
                    Station::NoPrices => ("No prices available", None, None, None),
                };

                row.col(|ui| {
                    ui.add(Label::new(status).wrap(false));
                });

                for ele in [e5, e10, diesel].iter() {
                    row.col(|ui| {
                        ui.add(
                            Label::new(ele.map_or("N/A".to_string(), |price| price.to_string()))
                                .wrap(false),
                        );
                    });
                }
            })
        });
}

fn powered_by_egui_and_eframe(ui: &mut egui::Ui) {
    ui.horizontal(|ui| {
        ui.spacing_mut().item_spacing.x = 0.0;
        ui.label("Powered by ");
        ui.hyperlink_to("egui", "https://github.com/emilk/egui");
        ui.label(" and ");
        ui.hyperlink_to(
            "eframe",
            "https://github.com/emilk/egui/tree/master/crates/eframe",
        );
        ui.label(".");
    });
}
