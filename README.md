# Tanker Koenig

Small egui app fetching and represent petrol price data from [Tankerkönig-API](https://creativecommons.tankerkoenig.de/).
This is mostly to test things out and not meant for actual usages.
As such the the queried petrol stations are also hard coded.
